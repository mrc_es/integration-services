"""
A Mysql connection factory that reading from env variables the user, password, and jdbcurl
can connect to a MariaDB database in google cloud
"""

import os
import mysql.connector
from mysql.connector import errorcode


class MySqlConnectionFactory:

    def __init__(self):
        self.user = os.environ.get('MYSQL_USER')
        self.password = os.environ.get('MYSQL_PASSWORD')
        self.database = os.environ.get('MYSQL_DATABASE')
        self.host = os.environ.get('MYSQL_HOST')
        self.port = os.environ.get('MYSQL_PORT')

    def get_connection(self):
        try:
            cnx = mysql.connector.connect(user=self.user, password=self.password,
                                          host=self.host, port=self.port,
                                          database=self.database)
            return cnx
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your credentials")
                raise err
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
                raise err
            else:
                print(err)
                raise err
        except Exception as e:
            print(e)
            raise e
