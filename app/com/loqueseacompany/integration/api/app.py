from flask import Flask, jsonify, request
from app.com.loqueseacompany.integration.api.storage.mysql.MySqlConnectionFactory import MySqlConnectionFactory

app = Flask(__name__)


@app.route('/api/v1.0/health', methods=['GET'])
def health():
    return jsonify({'status': 'OK'})


@app.route('/api/v1.0/echo', methods=['POST'])
def echo():
    return jsonify(request.json)


@app.route('/api/v1.0/my_sql/health', methods=['GET'])
def check_mysql_connection_status():
    try:
        connection = MySqlConnectionFactory().get_connection()
        connection.close()
        return jsonify({'status': 'OK'})
    except Exception as e:
        return jsonify({'status': 'ERROR', 'message': str(e)})
